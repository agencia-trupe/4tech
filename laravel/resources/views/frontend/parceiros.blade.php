@extends('frontend.common.template')

@section('content')

    <div class="main">
        <div class="center">
            <div class="content-wrapper">
                <div class="title">
                    <h1>Parceiros</h1>
                </div>
                <div class="content clientes">
                    <p>ALGUNS DE NOSSOS PARCEIROS</p>
                    <div class="imagens">
                        @foreach($parceiros as $parceiro)
                        <div class="cliente">
                            <img src="{{ asset('assets/img/parceiros/'.$parceiro->imagem) }}" alt="">
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
