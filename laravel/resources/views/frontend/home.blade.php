@extends('frontend.common.template')

@section('content')

    <div class="home">
        <div class="center center-banners">
            <div class="banners">
                @foreach($banners as $banner)
                <div class="banner" style="background-image:url({{ asset('assets/img/banners/'.$banner->imagem) }}">
                    @if($banner->texto)
                    <div class="wrapper">
                        <div class="texto">
                            {!! $banner->texto !!}
                        </div>
                    </div>
                    @endif
                </div>
                @endforeach
            </div>
        </div>
        <div class="chamadas">
            <div class="center">
                @foreach(range(1, 3) as $i)
                <div class="chamada">
                    <div class="imagem" style="background-image:url({{ asset('assets/img/layout/chamada-'.$i.'.png') }})">
                    </div>
                    <h3>{{ $home->{'chamada_'.$i.'_titulo'} }}</h3>
                    <p>{!! $home->{'chamada_'.$i.'_texto'} !!}</p>
                </div>
                @endforeach
            </div>
        </div>

        @if($home->noticiaDestaque)
        <div class="noticia-destaque">
            <a href="{{ route('noticias', $home->noticiaDestaque->slug) }}" class="center">
                <div class="imagem"></div>
                <div class="texto">
                    <h3>{{ $home->noticiaDestaque->titulo }}</h3>
                    <p>{!! $home->noticia_em_destaque_chamada !!}</p>
                </div>
            </a>
        </div>
        @endif
    </div>

@endsection
