    @if(! isset($exception) || $exception->getStatusCode() != 404)
    <div class="newsletter">
        <div class="center">
            <form action="{{ route('newsletter') }}" id="form-newsletter">
                <p>CADASTRE-SE PARA RECEBER UM CONTATO DA NOSSA EQUIPE</p>
                <div class="box">
                    <input type="email" name="newsletter_email" placeholder="e-mail" required>
                    <button></button>
                </div>
            </form>
            <img src="{{ asset('assets/img/layout/newsletter.png') }}" alt="">
        </div>
    </div>
    @endif

    <footer>
        <div class="center">
            <div class="col telefone">
                <h3>4Tech IT</h3>
                <p>{{ $contato->telefone }}</p>
            </div>
            <div class="col links">
                <a href="{{ route('home') }}"><span>_</span> HOME</a>
                <a href="{{ route('empresa') }}"><span>_</span> EMPRESA</a>
                <a href="{{ route('servicos') }}"><span>_</span> SERVIÇOS</a>
                <a href="{{ route('clientes') }}"><span>_</span> CLIENTES</a>
            </div>
            <div class="col links">
                <a href="{{ route('parceiros') }}"><span>_</span> PARCEIROS</a>
                <a href="{{ route('noticias') }}"><span>_</span> NOTÍCIAS</a>
                <a href="{{ route('contato') }}"><span>_</span> CONTATO</a>
            </div>
            <div class="col copyright">
                <p>
                    © {{ date('Y') }} {{ config('app.name') }}
                    <br>
                    Todos os direitos reservados.
                </p>
                <p>
                    <a href="http://www.trupe.net" target="_blank">Criação de sites</a>:<br>
                    <a href="http://www.trupe.net" target="_blank">Trupe Agência Criativa</a>
                </p>
            </div>
        </div>
    </footer>
