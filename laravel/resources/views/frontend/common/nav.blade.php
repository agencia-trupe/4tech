<a href="{{ route('home') }}" @if(Tools::routeIs('home')) class="active" @endif>Home</a>
<a href="{{ route('empresa') }}" @if(Tools::routeIs('empresa')) class="active" @endif>Empresa</a>
<a href="{{ route('servicos') }}" @if(Tools::routeIs('servicos')) class="active" @endif>Serviços</a>
<a href="{{ route('clientes') }}" @if(Tools::routeIs('clientes')) class="active" @endif>Clientes</a>
<a href="{{ route('parceiros') }}" @if(Tools::routeIs('parceiros')) class="active" @endif>Parceiros</a>
<a href="{{ route('noticias') }}" @if(Tools::routeIs('noticias')) class="active" @endif>Notícias</a>
<a href="{{ route('contato') }}" @if(Tools::routeIs('contato')) class="active" @endif>Contato</a>
