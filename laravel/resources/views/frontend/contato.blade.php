@extends('frontend.common.template')

@section('content')

    <div class="main">
        <div class="center">
            <div class="content-wrapper">
                <div class="title">
                    <h1>Contato</h1>
                </div>
                <div class="content contato">
                    <h2>{{ $contato->telefone }}</h2>
                    <p>FALE CONOSCO</p>

                    @if($errors->any())
                        <div class="flash flash-erro">
                            @foreach($errors->all() as $error)
                            {!! $error !!}<br>
                            @endforeach
                        </div>
                    @endif

                    @if(session('enviado'))
                        <div class="flash flash-sucesso">
                            Mensagem enviada com sucesso!
                        </div>
                    @endif

                    <form action="{{ route('contato.post') }}" method="POST">
                        {!! csrf_field() !!}

                        <input type="text" name="nome" placeholder="nome" value="{{ old('nome') }}" required>
                        <input type="email" name="email" placeholder="e-mail" value="{{ old('email') }}" required>
                        <input type="text" name="telefone" value="{{ old('telefone') }}" placeholder="telefone">
                        <textarea name="mensagem" placeholder="mensagem" required>{{ old('mensagem') }}</textarea>
                        <input type="submit" value="ENVIAR">
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
