<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Servicos;

class ServicosController extends Controller
{
    public function index()
    {
        $servicos = Servicos::first();

        return view('frontend.servicos', compact('servicos'));
    }
}
