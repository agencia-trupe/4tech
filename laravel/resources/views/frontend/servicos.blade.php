@extends('frontend.common.template')

@section('content')

    <div class="main">
        <div class="center">
            <div class="content-wrapper">
                <div class="title">
                    <h1>Serviços</h1>
                </div>
                <div class="content institucional">
                    {!! $servicos->texto !!}
                </div>
            </div>
        </div>
    </div>

@endsection
