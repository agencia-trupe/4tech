<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class HomeRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'chamada_1_titulo' => 'required',
            'chamada_1_texto' => 'required',
            'chamada_2_titulo' => 'required',
            'chamada_2_texto' => 'required',
            'chamada_3_titulo' => 'required',
            'chamada_3_texto' => 'required',
            'noticia_em_destaque' => '',
            'noticia_em_destaque_chamada' => '',
        ];
    }
}
