@extends('frontend.common.template')

@section('content')

    <div class="main">
        <div class="center">
            <div class="content-wrapper">
                <div class="title">
                    <h1>Notícias</h1>
                </div>
                <div class="content noticias institucional">
                    <div class="noticias-show">
                        <h2>
                            {{ $noticia->titulo }}
                            <span class="data">{{ $noticia->data }}</span>
                        </h2>

                        {!! $noticia->texto !!}

                        <div class="noticias-nav">
                            @if($anterior)
                            <a href="{{ route('noticias', $anterior->slug) }}" class="anterior">anterior</a>
                            @endif
                            <a href="{{ route('noticias') }}" class="todas">ver todas as notícias</a>
                            @if($proxima)
                            <a href="{{ route('noticias', $proxima->slug) }}" class="proxima">próxima</a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
