<?php

use Illuminate\Database\Seeder;

class HomeSeeder extends Seeder
{
    public function run()
    {
        DB::table('home')->insert([
            'chamada_1_titulo' => '',
            'chamada_1_texto' => '',
            'chamada_2_titulo' => '',
            'chamada_2_texto' => '',
            'chamada_3_titulo' => '',
            'chamada_3_texto' => '',
            'noticia_em_destaque' => null,
            'noticia_em_destaque_chamada' => '',
        ]);
    }
}
