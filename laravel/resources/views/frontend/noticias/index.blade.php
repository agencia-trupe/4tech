@extends('frontend.common.template')

@section('content')

    <div class="main">
        <div class="center">
            <div class="content-wrapper">
                <div class="title">
                    <h1>Notícias</h1>
                </div>
                <div class="content noticias">
                    <div class="noticias-index">
                        @foreach($noticias as $noticia)
                        <a href="{{ route('noticias', $noticia->slug) }}">
                            <span class="titulo">{{ $noticia->titulo }}</span>
                            <span class="data">{{ $noticia->data }}</span>
                        </a>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
