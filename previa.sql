-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: mysql
-- Tempo de geração: 27/11/2018 às 16:29
-- Versão do servidor: 8.0.2-dmr
-- Versão do PHP: 7.0.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `4tech`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `banners`
--

CREATE TABLE `banners` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `banners`
--

INSERT INTO `banners` (`id`, `ordem`, `imagem`, `texto`, `created_at`, `updated_at`) VALUES
(1, 0, 'img-banner-home_201811271626345Ac2ylArk7.png', 'Ajudamos a implantar<br />\r\na melhor solu&ccedil;&atilde;o ERP<br />\r\npara o seu neg&oacute;cio', '2018-11-27 16:26:35', '2018-11-27 16:26:35');

-- --------------------------------------------------------

--
-- Estrutura para tabela `clientes`
--

CREATE TABLE `clientes` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `configuracoes`
--

CREATE TABLE `configuracoes` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `keywords` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem_de_compartilhamento` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `analytics` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `configuracoes`
--

INSERT INTO `configuracoes` (`id`, `title`, `description`, `keywords`, `imagem_de_compartilhamento`, `analytics`, `created_at`, `updated_at`) VALUES
(1, '4Tech &middot; IT Solutions', '', '', '', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `contato`
--

CREATE TABLE `contato` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `contato`
--

INSERT INTO `contato` (`id`, `email`, `telefone`, `created_at`, `updated_at`) VALUES
(1, 'contato@trupe.net', '11 98410 9986', NULL, '2018-11-27 16:25:15');

-- --------------------------------------------------------

--
-- Estrutura para tabela `contatos_recebidos`
--

CREATE TABLE `contatos_recebidos` (
  `id` int(10) UNSIGNED NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mensagem` text COLLATE utf8_unicode_ci NOT NULL,
  `lido` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `empresa`
--

CREATE TABLE `empresa` (
  `id` int(10) UNSIGNED NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `empresa`
--

INSERT INTO `empresa` (`id`, `texto`, `created_at`, `updated_at`) VALUES
(1, '', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `home`
--

CREATE TABLE `home` (
  `id` int(10) UNSIGNED NOT NULL,
  `chamada_1_titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `chamada_1_texto` text COLLATE utf8_unicode_ci NOT NULL,
  `chamada_2_titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `chamada_2_texto` text COLLATE utf8_unicode_ci NOT NULL,
  `chamada_3_titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `chamada_3_texto` text COLLATE utf8_unicode_ci NOT NULL,
  `noticia_em_destaque` int(10) UNSIGNED DEFAULT NULL,
  `noticia_em_destaque_chamada` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `home`
--

INSERT INTO `home` (`id`, `chamada_1_titulo`, `chamada_1_texto`, `chamada_2_titulo`, `chamada_2_texto`, `chamada_3_titulo`, `chamada_3_texto`, `noticia_em_destaque`, `noticia_em_destaque_chamada`, `created_at`, `updated_at`) VALUES
(1, 'OTIMIZAÇÃO DE PROCESSOS', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque a aliquam ipsum, non pellentesque leo.', 'RESULTADOS FINANCEIROS', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque a aliquam ipsum, non pellentesque leo.', 'MAIOR COMPETITIVIDADE', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque a aliquam ipsum, non pellentesque leo.', 1, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce eget ipsum ut velit lacinia vulputate. Maecenas dui ex, placerat at mollis eget, sollicitudin eu mauris. Nullam imperdiet vehicula sagittis. Vivamus a vestibulum ipsum.', NULL, '2018-11-27 16:27:04');

-- --------------------------------------------------------

--
-- Estrutura para tabela `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2016_02_01_000000_create_users_table', 1),
('2016_03_01_000000_create_contato_table', 1),
('2016_03_01_000000_create_contatos_recebidos_table', 1),
('2017_09_01_163723_create_configuracoes_table', 1),
('2018_11_23_175901_create_banners_table', 1),
('2018_11_23_180056_create_empresa_table', 1),
('2018_11_23_180103_create_servicos_table', 1),
('2018_11_23_180301_create_clientes_table', 1),
('2018_11_23_180451_create_parceiros_table', 1),
('2018_11_26_092405_create_newsletter_table', 1),
('2018_11_26_093423_create_noticias_table', 1),
('2018_11_26_095323_create_home_table', 1);

-- --------------------------------------------------------

--
-- Estrutura para tabela `newsletter`
--

CREATE TABLE `newsletter` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `noticias`
--

CREATE TABLE `noticias` (
  `id` int(10) UNSIGNED NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `data` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `noticias`
--

INSERT INTO `noticias` (`id`, `slug`, `data`, `titulo`, `texto`, `created_at`, `updated_at`) VALUES
(1, 'exemplo', '2018-11-27', 'Exemplo', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce eget ipsum ut velit lacinia vulputate. Maecenas dui ex, placerat at mollis eget, sollicitudin eu mauris. Nullam imperdiet vehicula sagittis. Vivamus a vestibulum ipsum. Aliquam volutpat dui sit amet sem aliquam, non aliquam magna egestas. Etiam tempus lorem sit amet elementum dapibus. Ut posuere sagittis neque sed gravida. Suspendisse at turpis consectetur, fringilla sapien sit amet, bibendum velit. Mauris lobortis nibh vitae purus facilisis, sit amet efficitur nulla suscipit. Suspendisse justo leo, vestibulum ac metus quis, imperdiet semper felis. Integer scelerisque felis ac felis suscipit malesuada. Sed lorem erat, vulputate in tincidunt sit amet, pretium eu dolor. Praesent id tempor tortor, ut accumsan dolor.</p>\r\n\r\n<p>Nullam eleifend ultricies enim. Donec sed euismod ante. Morbi ornare urna nisl, sit amet mollis purus porttitor et. Morbi ut lacinia nulla. Nam fringilla ullamcorper augue, ac luctus lorem semper ornare. Pellentesque non tortor mattis, bibendum diam eget, laoreet felis. In id ipsum mauris. Suspendisse potenti. Ut nec fringilla magna. Nullam sit amet arcu non purus cursus imperdiet non nec magna. Quisque porta fringilla tempor. Aliquam congue tincidunt ligula. Nam facilisis leo eu urna feugiat, a elementum quam pretium. Maecenas imperdiet nulla at odio tristique, sit amet iaculis ipsum ullamcorper. Maecenas ipsum dolor, luctus quis dapibus at, iaculis at nisl. Nulla non ipsum risus.</p>\r\n\r\n<p>Ut auctor tellus quis libero sagittis facilisis. Etiam fermentum ipsum eros, non volutpat felis egestas in. Fusce in condimentum urna. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Integer ac est dui. Duis diam mauris, accumsan a fringilla nec, scelerisque non metus. Integer non consequat urna. Quisque at risus justo. Nulla facilisi. Vivamus non sapien ex. Sed tempus diam vitae magna suscipit suscipit. Nunc hendrerit tortor diam, vitae consequat sem commodo vel. In fringilla mollis pretium. Duis pretium, tellus vitae posuere aliquam, lacus arcu venenatis lorem, ac malesuada nisi risus eu nulla. In ac malesuada felis.</p>\r\n', '2018-11-27 16:26:59', '2018-11-27 16:26:59');

-- --------------------------------------------------------

--
-- Estrutura para tabela `parceiros`
--

CREATE TABLE `parceiros` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `servicos`
--

CREATE TABLE `servicos` (
  `id` int(10) UNSIGNED NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `servicos`
--

INSERT INTO `servicos` (`id`, `texto`, `created_at`, `updated_at`) VALUES
(1, '', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'trupe', 'contato@trupe.net', '$2y$10$vSAdBPDtZdIYdvG43WhUZODD82vFbZqu6NmMzJ8piGM6gGsN1OKWq', NULL, NULL, NULL);

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `configuracoes`
--
ALTER TABLE `configuracoes`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `contato`
--
ALTER TABLE `contato`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `contatos_recebidos`
--
ALTER TABLE `contatos_recebidos`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `empresa`
--
ALTER TABLE `empresa`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `home`
--
ALTER TABLE `home`
  ADD PRIMARY KEY (`id`),
  ADD KEY `home_noticia_em_destaque_foreign` (`noticia_em_destaque`);

--
-- Índices de tabela `newsletter`
--
ALTER TABLE `newsletter`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `newsletter_email_unique` (`email`);

--
-- Índices de tabela `noticias`
--
ALTER TABLE `noticias`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `parceiros`
--
ALTER TABLE `parceiros`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `servicos`
--
ALTER TABLE `servicos`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `banners`
--
ALTER TABLE `banners`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `clientes`
--
ALTER TABLE `clientes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `configuracoes`
--
ALTER TABLE `configuracoes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `contato`
--
ALTER TABLE `contato`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `contatos_recebidos`
--
ALTER TABLE `contatos_recebidos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `empresa`
--
ALTER TABLE `empresa`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `home`
--
ALTER TABLE `home`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `newsletter`
--
ALTER TABLE `newsletter`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `noticias`
--
ALTER TABLE `noticias`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `parceiros`
--
ALTER TABLE `parceiros`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `servicos`
--
ALTER TABLE `servicos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Restrições para dumps de tabelas
--

--
-- Restrições para tabelas `home`
--
ALTER TABLE `home`
  ADD CONSTRAINT `home_noticia_em_destaque_foreign` FOREIGN KEY (`noticia_em_destaque`) REFERENCES `noticias` (`id`) ON DELETE SET NULL;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
