@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2>
            Newsletter
            @if($registros->total())
            <small>({{ $registros->total() }} registro{{ $registros->total() != 1 ? 's' : '' }})</small>
            @endif

            <a href="{{ route('painel.newsletter.exportar') }}" class="btn btn-sm btn-info pull-right @if(!$registros->total()) disabled @endif">
                <span class="glyphicon glyphicon-download-alt" style="margin-right:10px;"></span>
                Exportar XLS
            </a>
        </h2>
    </legend>

    @if(!count($registros))
    <div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
    @else
    <table class="table table-striped table-bordered table-hover table-info">
        <thead>
            <tr>
                <th>Data de Cadastro</th>
                <th>E-mail</th>
                <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)
            <tr>
                <td>{{ $registro->created_at->format('d/m/Y H:i') }}</td>
                <td>
                    <button class="btn btn-info btn-sm clipboard" data-clipboard-text="{{ $registro->email }}" style="margin-right:5px;border:0;transition:background .3s">
                        <span class="glyphicon glyphicon-copy"></span>
                    </button>
                    {{ $registro->email }}
                </td>
                <td class="crud-actions">
                    {!! Form::open([
                        'route'  => ['painel.newsletter.destroy', $registro],
                        'method' => 'delete'
                    ]) !!}
                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    {{ $registros->links() }}
    @endif

@endsection
