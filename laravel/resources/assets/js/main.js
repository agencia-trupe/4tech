import AjaxSetup from './AjaxSetup';
import MobileToggle from './MobileToggle';

AjaxSetup();
MobileToggle();

$('.banners').cycle({
    slides: '>.banner'
});

$('#form-newsletter').submit(function(event) {
    event.preventDefault();

    var $form = $(this),
        $response = $form.find('.response');

    if ($form.hasClass('sending')) return false;

    $response.fadeOut('fast');
    $form.addClass('sending');

    $.ajax({
        type: 'POST',
        url: $form.attr('action'),
        data: {
            email: $form.find('input[name=newsletter_email]').val(),
        }
    }).done(function(data) {
        alert(data.message);
        $form[0].reset();
    }).fail(function(data) {
        var res = data.responseJSON,
            txt = res.email;
        alert(txt);
    }).always(function() {
        $form.removeClass('sending');
    });
});
