<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Home extends Model
{
    protected $table = 'home';

    protected $guarded = ['id'];

    public function noticiaDestaque()
    {
        return $this->belongsTo(Noticia::class, 'noticia_em_destaque');
    }
}
