<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ParceirosRequest;
use App\Http\Controllers\Controller;

use App\Models\Parceiro;

class ParceirosController extends Controller
{
    public function index()
    {
        $registros = Parceiro::ordenados()->get();

        return view('painel.parceiros.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.parceiros.create');
    }

    public function store(ParceirosRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Parceiro::upload_imagem();

            Parceiro::create($input);

            return redirect()->route('painel.parceiros.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Parceiro $registro)
    {
        return view('painel.parceiros.edit', compact('registro'));
    }

    public function update(ParceirosRequest $request, Parceiro $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Parceiro::upload_imagem();

            $registro->update($input);

            return redirect()->route('painel.parceiros.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Parceiro $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.parceiros.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
