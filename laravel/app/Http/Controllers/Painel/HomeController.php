<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\HomeRequest;
use App\Http\Controllers\Controller;

use App\Models\Home;
use App\Models\Noticia;

class HomeController extends Controller
{
    public function index()
    {
        $registro = Home::first();
        $noticias = Noticia::ordenados()->lists('titulo', 'id');

        return view('painel.home.edit', compact('registro', 'noticias'));
    }

    public function update(HomeRequest $request, Home $registro)
    {
        try {
            $input = $request->all();

            if (! $request->has('noticia_em_destaque')) {
                $input['noticia_em_destaque'] = null;
                $input['noticia_em_destaque_chamada'] = '';
            }

            $registro->update($input);

            return redirect()->route('painel.home.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
