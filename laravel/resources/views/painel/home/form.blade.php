@include('painel.common.flash')

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('chamada_1_titulo', 'Chamada 1 Título') !!}
            {!! Form::text('chamada_1_titulo', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('chamada_1_texto', 'Chamada 1 Texto') !!}
            {!! Form::textarea('chamada_1_texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('chamada_2_titulo', 'Chamada 2 Título') !!}
            {!! Form::text('chamada_2_titulo', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('chamada_2_texto', 'Chamada 2 Texto') !!}
            {!! Form::textarea('chamada_2_texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('chamada_3_titulo', 'Chamada 3 Título') !!}
            {!! Form::text('chamada_3_titulo', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('chamada_3_texto', 'Chamada 3 Texto') !!}
            {!! Form::textarea('chamada_3_texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>
    </div>
</div>

<hr>

<div class="form-group">
    {!! Form::label('noticia_em_destaque', 'Notícia em Destaque') !!}
    {!! Form::select('noticia_em_destaque', $noticias, null, ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}
</div>

<div class="form-group">
    {!! Form::label('noticia_em_destaque_chamada', 'Notícia em Destaque Chamada') !!}
    {!! Form::textarea('noticia_em_destaque_chamada', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
