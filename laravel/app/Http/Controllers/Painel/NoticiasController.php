<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\NoticiasRequest;
use App\Http\Controllers\Controller;

use App\Models\Noticia;

class NoticiasController extends Controller
{
    public function index()
    {
        $registros = Noticia::ordenados()->paginate(10);

        return view('painel.noticias.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.noticias.create');
    }

    public function store(NoticiasRequest $request)
    {
        try {

            $input = $request->all();

            Noticia::create($input);

            return redirect()->route('painel.noticias.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Noticia $registro)
    {
        return view('painel.noticias.edit', compact('registro'));
    }

    public function update(NoticiasRequest $request, Noticia $registro)
    {
        try {

            $input = $request->all();

            $registro->update($input);

            return redirect()->route('painel.noticias.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Noticia $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.noticias.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
