@extends('frontend.common.template')

@section('content')

    <div class="main">
        <div class="center">
            <div class="content-wrapper">
                <div class="title">
                    <h1>Empresa</h1>
                </div>
                <div class="content institucional">
                    {!! $empresa->texto !!}
                </div>
            </div>
        </div>
    </div>

@endsection
