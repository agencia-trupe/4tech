@extends('frontend.common.template')

@section('content')

    <div class="main">
        <div class="center">
            <div class="content-wrapper">
                <div class="title">
                    <h1>Clientes</h1>
                </div>
                <div class="content clientes">
                    <p>ALGUNS DE NOSSOS CLIENTES</p>
                    <div class="imagens">
                        @foreach($clientes as $cliente)
                        <div class="cliente">
                            <img src="{{ asset('assets/img/clientes/'.$cliente->imagem) }}" alt="">
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
