<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Noticia;

class NoticiasController extends Controller
{
    public function index(Noticia $noticia)
    {
        $noticias = Noticia::ordenados()->get();

        if ($noticia->exists) {
            $key = $noticias->search(function($el) use ($noticia) {
                return $el->id == $noticia->id;
            });
            $anterior = $noticias->get($key+1);
            $proxima  = $noticias->get($key-1);
            return view('frontend.noticias.show', compact('noticia', 'anterior', 'proxima'));
        }

        return view('frontend.noticias.index', compact('noticias'));
    }
}
