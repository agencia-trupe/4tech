<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHomeTable extends Migration
{
    public function up()
    {
        Schema::create('home', function (Blueprint $table) {
            $table->increments('id');
            $table->string('chamada_1_titulo');
            $table->text('chamada_1_texto');
            $table->string('chamada_2_titulo');
            $table->text('chamada_2_texto');
            $table->string('chamada_3_titulo');
            $table->text('chamada_3_texto');
            $table->integer('noticia_em_destaque')->unsigned()->nullable();
            $table->text('noticia_em_destaque_chamada');
            $table->timestamps();

            $table->foreign('noticia_em_destaque')->references('id')->on('noticias')->onDelete('set null');
        });
    }

    public function down()
    {
        Schema::drop('home');
    }
}
