<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class NewsletterRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'email' => 'required|email|unique:newsletter,email'
        ];
    }

    public function messages()
    {
        return [
            'email.required' => 'Preencha seu e-mail.',
            'email.email'    => 'Insira um e-mail válido.',
            'email.unique'   => 'Este e-mail já está cadastrado.'
        ];
    }
}
